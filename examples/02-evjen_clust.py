#!/usr/bin/env python3

import numpy
from sys import path
# from numba import njit
path.append("..")
from chargedel import uc
from chargedel import sums
from chargedel import gen_array
from chargedel import io


def RMS(data_list):
    return numpy.sum((numpy.array(data_list) -
                      numpy.average(data_list))**2)/len(data_list)


# @njit
def idx_negative(array, indices=(0, 1, 2)):
    ZERO = 1e-14
    negative = numpy.zeros(array.shape[0], dtype=bool)
    indices = numpy.array(indices)
    for i in range(array.shape[0]):
        is_negative = True
        for j in range(indices.shape[0]):
            if array[i, j] > ZERO:  # if j-th coord of the i-th ion > 0
                is_negative = False
        negative[i] = is_negative
    return negative


base = "Li3N"
cif_file = "./data-02/" + base + ".cif"
my_cells = 16

uc1 = uc.unitcell(ciffile=cif_file, verbose=True)
# Rotate unit cell 30 deg around z, such that symmetry axes are along x and y
uc1.rotate_cartesian_abc_(30)
uc1.print()

(pc_array_xyz, pc_array_charges, pc_array_labels) = \
    gen_array.gen_pc(uc1, n_cells=my_cells, extra_layers=3,
                     apply_evjen=True, sort=True)
io.write_xyz(base + ".box.xyz", pc_array_xyz, pc_array_charges,
             pc_array_labels)

clust_xyz, clust_labels = gen_array.find_cluster(pc_array_xyz, pc_array_labels,
                                                 centers=[["Li"], ["N"]],
                                                 spheres=[3.66, 2.65])
io.write_xyz(base + ".clust.xyz", clust_xyz, numpy.zeros(len(clust_labels)),
             clust_labels)

random_shift_list = []
evjen_shift_list = []
n_random = 4
for xyz in clust_xyz:
    evjen_pot = sums.direct_sum(xyz, pc_array_xyz,
                                pc_array_charges)
    ewald_pot = sums.ewald_sum(xyz, uc1)
    evjen_shift_list.append(evjen_pot - ewald_pot)
    for i in range(n_random):
        random_xyz = numpy.random.rand(3)
        evjen_pot = sums.direct_sum(xyz + random_xyz,
                                    pc_array_xyz, pc_array_charges)
        ewald_pot = sums.ewald_sum(xyz + random_xyz, uc1)
        random_shift_list.append(evjen_pot - ewald_pot)

print("Average shift and RMS errors of the Evjen scheme compare to Ewald sum")
print("For {} cluster sites: {} {}".format(len(clust_xyz),
                                           numpy.average(evjen_shift_list),
                                           RMS(evjen_shift_list)))
print("For {} random points: {} {}".format(len(clust_xyz)*n_random,
                                           numpy.average(random_shift_list),
                                           RMS(random_shift_list)))
joined_list = evjen_shift_list + random_shift_list
print("For clust+random: {} {}".format(numpy.average(joined_list),
                                       RMS(joined_list)))
  
(pc_array_xyz, pc_array_charges, pc_array_labels) = \
    gen_array.subtract_cluster(clust_xyz, pc_array_xyz,
                               pc_array_charges, pc_array_labels,
                               cluster_labels=clust_labels)
io.write_xyz(base + ".evjen.xyz", pc_array_xyz, pc_array_charges,
             pc_array_labels)
io.write_molpro(base + ".evjen.lat", pc_array_xyz, pc_array_charges)


# Detect symmetry
symm_operations = gen_array.detect_symmetry(pc_array_xyz)
print(len(symm_operations), symm_operations)
if len(symm_operations) == 7:  # D2h
    print("Only 1 octant is symmetry unique, keep sites with all coords negative")
    unique = idx_negative(pc_array_xyz, indices=(0, 1, 2))
    sym_xyz = pc_array_xyz[unique]
    sym_cha = pc_array_charges[unique]
    sym_lab = pc_array_labels[unique]
    io.write_xyz(base + ".evjen.sym.xyz", sym_xyz, sym_cha, sym_lab)
    io.write_molpro(base + ".evjen.sym.lat", sym_xyz, sym_cha)
    unique_clust = idx_negative(clust_xyz)
    sym_clust_xyz = clust_xyz[unique_clust]
    sym_clust_lab = clust_labels[unique_clust]
    io.write_xyz(base + ".clust.sym.xyz", sym_clust_xyz, numpy.zeros(len(sym_clust_lab)),
                 sym_clust_lab)
