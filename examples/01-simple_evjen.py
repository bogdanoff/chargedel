#!/usr/bin/env python3

import numpy
from sys import path
path.append("..")
from chargedel import uc
from chargedel import sums
from chargedel import gen_array
from chargedel import io

base = "LaNiO2"
uc_file = "./data-01/" + base + ".uc"
my_cells = 8

uc1 = uc.unitcell(ewaldinp=uc_file)
# uc1.print()
(form_array_xyz, form_array_charges, form_array_labels) = \
    gen_array.gen_pc(uc1, n_cells=my_cells, extra_layers=3,
                     apply_evjen=False, sort=True)
# io.write_xyz(base + ".formal2.xyz", form_array_xyz,
#              form_array_charges, form_array_labels)

(pc_array_xyz, pc_array_charges, pc_array_labels) = \
    gen_array.gen_pc(uc1, n_cells=my_cells, extra_layers=3,
                     apply_evjen=True, sort=True)

clust_file = "./data-01/clust17.xyz"
clust_xyz, clust_charges, clust_labels = io.read_xyz(clust_file)

form_error = evjen_error = 0
form_shift = evjen_shift = 0
form_shift_list = []
evjen_shift_list = []
# for xyz in ([1, 1, 1], [0, 1, 1], [0, 0, 1]):
for i, xyz in enumerate(clust_xyz):
    formal_pot = sums.direct_sum(xyz, form_array_xyz,
                                 form_array_charges)
    evjen_pot = sums.direct_sum(xyz, pc_array_xyz,
                                pc_array_charges)
    ewald_pot = sums.ewald_sum(xyz, uc1)
    if i == 0:
        form_shift = formal_pot - ewald_pot
        evjen_shift = evjen_pot - ewald_pot
        print("Define constant shifts using the first ion")
        print("For formal sum:", form_shift)
        print("For Ejven scheme:", evjen_shift)
    print(clust_labels[i], str(xyz))
    print("Ewald sum ", ewald_pot)
    ierr_form = formal_pot - ewald_pot - form_shift
    form_error += ierr_form*ierr_form
    form_shift_list.append(formal_pot - ewald_pot)
    print("Formal charges sum %s, diff to Ewald including approx shift %s" %
          (formal_pot,  # formal_pot - ewald_pot,
           ierr_form))
    ierr_evjen = evjen_pot - ewald_pot - evjen_shift
    evjen_error += ierr_evjen*ierr_evjen
    evjen_shift_list.append(evjen_pot - ewald_pot)
    print("Evjen charges sum %s, diff to Ewald including approx shift %s" %
          (evjen_pot,  # evjen_pot - ewald_pot,
           ierr_evjen))
print("Approx shifts were defined using the first ion")
print("For formal sum:", form_shift)
print("For Ejven scheme:", evjen_shift)
form_error = numpy.sqrt(form_error)/len(clust_xyz)
evjen_error = numpy.sqrt(evjen_error)/len(clust_xyz)
print("Mean square eroror with that shift")
print("For formal sum:", form_error)
print("For Ejven scheme:", evjen_error)

print("Average shifts and RMS errors")
print("For formal sum:", numpy.average(form_shift_list),
      numpy.sum((numpy.array(form_shift_list) -
                 numpy.average(form_shift_list))**2)/len(form_shift_list))
print("For Evjen sum:", numpy.average(evjen_shift_list),
      numpy.sum((numpy.array(evjen_shift_list) -
                 numpy.average(evjen_shift_list))**2)/len(evjen_shift_list))

# io.write_xyz(base + ".evjen.xyz", pc_array_xyz,
#              pc_array_charges, pc_array_labels)
# io.write_molpro(base + ".evjen2.lat", pc_array_xyz,
#                 pc_array_charges)

(pc_array_xyz, pc_array_charges, pc_array_labels) = \
    gen_array.subtract_cluster(clust_xyz, pc_array_xyz,
                               pc_array_charges, pc_array_labels,
                               cluster_labels=clust_labels)
io.write_xyz(base + ".evjen.xyz", pc_array_xyz, pc_array_charges,
             pc_array_labels)
io.write_molpro(base + ".evjen.lat", pc_array_xyz, pc_array_charges)
