#!/usr/bin/env python3

# from sys import exit
import numpy
from sys import path
path.append("..")
from chargedel import uc
from chargedel import sums
from chargedel import gen_array
from chargedel import io

"""
Film:
LaSrCuO4 structure, for simplicity no Sr ions => LaCuO4
La-La "out of plane" difference in z direction : 2.9317
apical O - apical O distance divided by 2 : 2.4069
a = b = 3.77470, we replace it by 3.75576 as in substrate
c = 13.23920
z_La = 0.562930784

Substrate:
LaSrAlO3, for the moment take SrAlO3 with "Sr 2.5+"
Sr-Sr "out of plane" diff z: 2.7474
apical O - apical O divided by 2 : 2.0600
a = b = 3.75576
c = 12.63770
z_Sr = 12.36346191 (c - z_Sr =  0.27423809)

Interface
Sr-La "out of plane" diff z : (2.9317 + 2.7474) / 2 = 2.83955
film upshift = 12.63770 - 0.27423809 - 0.562930784 + 2.83955 =
             = 14.640081126 approx= 14.64
film upshift over c_film = 1.10581312511 approx= 1.1058
"""

#  prepared such that top of the unit cell was defined to be the interface
#  and it is deep enough to be considered bulk
subs_cif = "./data-05/LSAO_substrate_deep.cif"

#  prepared such that bottom of the unit cell was defined to be the interface
#  and has the maximum film thickness
film_cif = "./data-05/LSCO_film_thickest.cif"

subs_uc = uc.unitcell(ciffile=subs_cif)

# Here we set Sr charge to be 2.5 (mixture of Sr and La)
# there are be no charges values in the example cif file
subs_charges = {"Sr": 2.5, "O": -2.0, "Al": 3.0}
subs_uc.set_charges(subs_charges)

# film_uc = uc.unitcell(ciffile=film_cif)

# c axis of the film can be different depending on the thickness
c_thick_dep = {1: 13.12045, 2: 13.20447, 4: 13.24132, 8: 13.27392}
film_thickness = 4
film_cif_dict = uc.read_cif(film_cif)
film_cif_dict["c"] = c_thick_dep[film_thickness]*4  # there are 4 unit cells in the cif file
film_uc = uc.unitcell(data_dict=film_cif_dict)
film_uc.print()

film_charges = {"O": -2.0, "Cu": 2.0, "La": 3.0}
film_uc.set_charges(film_charges)


#  We want to have some vacuum just to ensure that the Evjen procedure does not
#  change the top film charges
vacuum = numpy.array((0.0, 0.0, 1.0))

join_uc = uc.unitcell()
join_uc.abc_cartesian = numpy.vstack([subs_uc.abc_cartesian[0:2],
                                      subs_uc.abc_cartesian[2] +
                                      film_uc.abc_cartesian[2] + vacuum])
join_uc.abc_cartesian = numpy.around(join_uc.abc_cartesian, decimals=12)

#  z component of Sr and La NN. One needs to look into cif files and find
#  correct position.  Here it is between ions 2 and 1 (starting from zero),
#  substrate and film have supercells of 10 and 4 uc's made with VESTA
#  respectively
subs_nUC = 10
film_nUC = 4
SrLa1_id = 3 - 1
SrLa2_id = 2 - 1
uc_id = 1 - 1
z_subs_Sr_Sr = (subs_uc.ion_cartesian[SrLa1_id*subs_nUC + uc_id, 2] -  # zSr1 -
                subs_uc.ion_cartesian[SrLa2_id*subs_nUC + uc_id, 2])   # zSr2
z_film_La_La = (film_uc.ion_cartesian[SrLa1_id*film_nUC + uc_id, 2] -
                film_uc.ion_cartesian[SrLa2_id*film_nUC + uc_id, 2])
z_join_Sr_La = (z_subs_Sr_Sr + z_film_La_La) / 2

z_subs_top_Sr = numpy.max(
    subs_uc.ion_cartesian[:, 2][subs_uc.ion_labels == 'Sr'])
z_film_bottom_La = numpy.min(
    film_uc.ion_cartesian[:, 2][film_uc.ion_labels == 'La'])

#  Find how much we need to shift up the film to get the correct Sr - La z
shift_film_z = z_subs_top_Sr - z_film_bottom_La + z_join_Sr_La

#  Align film ions on top of the substrate
film_xyz_shifted = film_uc.ion_cartesian + numpy.array([0, 0, shift_film_z])

#  We need to translate substrate ions at z=0 by c_subs to the interface
list_to_translate = subs_uc.ion_cartesian[:, 2] == 0.0
trans_cartesian = (subs_uc.ion_cartesian[list_to_translate] +
                   subs_uc.abc_cartesian[2])
trans_labels = subs_uc.ion_labels[list_to_translate]
trans_charges = subs_uc.ion_charges[list_to_translate]

#  Fill up the joined unitcell (a,b,c) should be assigned at this point
join_uc.ion_cartesian = numpy.vstack((subs_uc.ion_cartesian, trans_cartesian,
                                      film_xyz_shifted)).round(decimals=12)
join_uc.ion_labels = numpy.hstack((subs_uc.ion_labels, trans_labels,
                                   film_uc.ion_labels))
join_uc.ion_charges = numpy.hstack((subs_uc.ion_charges, trans_charges,
                                    film_uc.ion_charges))
io.write_xyz("join_uc.xyz", join_uc.ion_cartesian, join_uc.ion_charges,
             join_uc.ion_labels)

ab_repeat = 20
my_cells = [ab_repeat, ab_repeat, 1]

(evjen_array_xyz, evjen_array_charges, evjen_array_labels) = \
    gen_array.gen_pc(join_uc, n_cells=my_cells, extra_layers=4,
                     apply_evjen=True, sort=True)

#  At the moment gen_array.gen_pc can only do symmetric charges, we choose the
#  bottom of the 2 join_uc along z (z < 0) and shift it up by c_join

take_only = evjen_array_xyz[:, 2] < 0
evjen_array_xyz = evjen_array_xyz[take_only] + join_uc.abc_cartesian[2]
evjen_array_charges = evjen_array_charges[take_only]
evjen_array_labels = evjen_array_labels[take_only]
io.write_xyz("join_evjen.xyz", evjen_array_xyz, evjen_array_charges,
             evjen_array_labels)


# xyz_O1 = (film_uc.ion_cartesian[O_lower_id*film_nUC + 0] +
#           numpy.array([0, 0, shift_film_z])).round(decimals=12)
# print(xyz_O1)
# index_O1 = (O_lower_id*film_nUC + 0 + subs_uc.ion_labels.shape[0] +
#             trans_labels.shape[0])
# print(join_uc.ion_cartesian[index_O1])
# frac_O1 = join_uc.ion_fractional[join_uc.ion_cartesian.round(decimals=12) ==
#                                  xyz_O1]
# print(frac_O1)
# frac_O1 = join_uc.ion_fractional[index_O1]
# print(frac_O1)

# xy_shift = list(film_uc.ion_fractional[O_lower_id*film_nUC + 0][0:2]) + \
#     [(-film_uc.abc_cartesian[2, 2] - vacuum[2])/join_uc.abc_cartesian[2, 2]]

# Set new x0 and y0 to be from the O_lower_id

#  In the film UC, bridging oxygens have labels (8 and 9) or (7 and 10)
# O_lower_id = 7 - 1

max_thickness = film_thickness*2
O_lower = [8-1, 7-1]
for O_index in range(2):
    O_lower_id = O_lower[O_index]
    xy0_shift = list(film_uc.ion_fractional[O_lower_id*film_nUC
                                            + 0][0:2]) + [0.0]
    join_uc.shift_origin_frac_(xy0_shift)

    (evjen_array_xyz, evjen_array_charges, evjen_array_labels) = \
        gen_array.gen_pc(join_uc, n_cells=my_cells, extra_layers=4,
                         apply_evjen=True, sort=True)
    # back shift join_uc for further use
    join_uc.shift_origin_frac_([-coor for coor in xy0_shift])

    # At the moment gen_array.gen_pc can only do symmetric charges, we choose
    # the bottom of the 2 join_uc along z (z < 0) and shift it up by join_uc.c
    take_only = evjen_array_xyz[:, 2] < 0
    evjen_array_xyz = evjen_array_xyz[take_only] + join_uc.abc_cartesian[2]
    evjen_array_charges = evjen_array_charges[take_only]
    evjen_array_labels = evjen_array_labels[take_only]
    io.write_xyz("join_evjenO"+str(O_lower_id+1)+".xyz", evjen_array_xyz,
                 evjen_array_charges, evjen_array_labels)
    # Find releven CuO layers, with our choice they have Cu[0,y,z]
    onlyCu = evjen_array_xyz[evjen_array_labels == "Cu"]
    zCu_x0 = numpy.unique(onlyCu[onlyCu[:, 0] == 0][:, 2])
    layer_chop = (zCu_x0[1] - zCu_x0[0]) / 2
    for (layer, shift) in enumerate(zCu_x0):
        abs_layer = O_index+1 + 2*layer
        print(abs_layer, shift)
        evjen_array_shift = evjen_array_xyz + numpy.array((0, 0, -shift))
        # io.write_xyz(
        #     "join_evjen_layer"+str(abs_layer)+".xyz",
        #     evjen_array_shift, evjen_array_charges, evjen_array_labels)
        for chop in range(max_thickness-abs_layer+1):
            within_thick = evjen_array_shift[:, 2] <= layer_chop*(0.5+chop)
            evjen_array_chop = evjen_array_shift[within_thick]
            evjen_array_charges_chop = evjen_array_charges[within_thick]
            evjen_array_labels_chop = evjen_array_labels[within_thick]
            # io.write_xyz(
            #     "join_evjen_thick" + str(abs_layer+chop) +
            #     "_layer" + str(abs_layer) + ".xyz",
            #     evjen_array_chop, evjen_array_charges_chop,
            #     evjen_array_labels_chop)
            clust_xyz, clust_lab = gen_array.find_cluster(
                evjen_array_chop, evjen_array_labels_chop)
            io.write_xyz("clust_thick" + str(abs_layer+chop) +
                         "_layer" + str(abs_layer) + ".xyz",
                         clust_xyz,
                         [0 for i in range(len(clust_lab))],
                         clust_lab)
            (pc_array_xyz, pc_array_charges, pc_array_labels) = \
                gen_array.subtract_cluster(clust_xyz, evjen_array_chop,
                                           evjen_array_charges_chop,
                                           evjen_array_labels_chop,
                                           cluster_labels=clust_lab)
            io.write_xyz(
                "PC_thick" + str(abs_layer+chop) +
                "_layer" + str(abs_layer) + ".xyz",
                pc_array_xyz, pc_array_charges, pc_array_labels)
            io.write_molpro(
                "PC_thick" + str(abs_layer+chop) +
                "_layer" + str(abs_layer) + ".lat",
                pc_array_xyz, pc_array_charges)

# Now we should generate all possible thicknesses and origin_z position.
# To do so we should know all z where we have Cu and all z where to chop,
# which are just above apical O's.
# It could be that two shifts are needed, because Cu layers alternate

#  Generate cluster similar to jmol language

#  Cut cluster from the full set of charges and save resulting charges

#  Remove PC's above z's of according to number of layers. This way we will
#  have new setup, for which all possible layers should be computed

##########

# exit()

##########

# base = "LaNiO2"
# uc_file = base + ".uc"
# my_cells = 8

# uc1 = uc.unitcell(ewaldinp=uc_file)
# # uc1.print()
# (form_array_xyz, form_array_charges, form_array_labels) = \
#     gen_array.gen_pc(uc1, n_cells=my_cells, extra_layers=3,
#                      apply_evjen=False, sort=True)
# # io.write_xyz(base + ".formal2.xyz", form_array_xyz,
# #              form_array_charges, form_array_labels)

# (pc_array_xyz, pc_array_charges, pc_array_labels) = \
#     gen_array.gen_pc(uc1, n_cells=my_cells, extra_layers=3,
#                      apply_evjen=True, sort=True)

# clust_xyz, clust_charges, clust_labels = io.read_xyz("clust17.xyz")

# form_shift = evjen_shift = 0
# # for xyz in ([1, 1, 1], [0, 1, 1], [0, 0, 1]):
# for i, xyz in enumerate(clust_xyz):
#     formal_pot = sums.direct_sum(xyz, form_array_xyz,
#                                  form_array_charges)
#     evjen_pot = sums.direct_sum(xyz, pc_array_xyz,
#                                 pc_array_charges)
#     ewald_pot = sums.ewald_sum(xyz, uc1)
#     if i == 0:
#         form_shift = formal_pot - ewald_pot
#         evjen_shift = evjen_pot - ewald_pot
#     print("Coordinate: " + str(xyz))
#     print("Ewald sum", ewald_pot)
#     print("Direct sum with formal charges and diff to Ewald", formal_pot,
#           formal_pot - ewald_pot, formal_pot - ewald_pot - form_shift)
#     print("Direct sum using Evjen charges and diff to Ewald", evjen_pot,
#           evjen_pot - ewald_pot, evjen_pot - ewald_pot - evjen_shift)

# # io.write_xyz(base + ".evjen.xyz", pc_array_xyz,
# #              pc_array_charges, pc_array_labels)
# # io.write_molpro(base + ".evjen2.lat", pc_array_xyz,
# #                 pc_array_charges)

# (pc_array_xyz, pc_array_charges, pc_array_labels) = \
#     gen_array.subtract_cluster(clust_xyz, pc_array_xyz,
#                                pc_array_charges, pc_array_labels,
#                                cluster_labels=clust_labels)
# io.write_xyz(base + ".evjen.xyz", pc_array_xyz,
#              pc_array_charges, pc_array_labels)
# io.write_molpro(base + ".evjen.lat", pc_array_xyz,
#                 pc_array_charges)
