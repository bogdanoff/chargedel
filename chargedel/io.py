#!/usr/bin/env python
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2019 Nikolay Bogdanov <n.bogdanov@inbox.lv>

import numpy


def write_xyz(filename, array_xyz, array_charges, array_labels):

    with open(filename, "w") as out:
        print(len(array_charges), "\n", sep='', file=out)
        for label, xyz, charge in zip(array_labels, array_xyz, array_charges):
            data = [label]+list(xyz)+[charge]
            # xyz_format = "{:2} {: 14.8f} {: 14.8f} {: 14.8f} {: 16.10f}"
            xyz_format = "{:2}" + 4*" {: 15.9f}"
            print(xyz_format.format(*data),
                  file=out)
    return


def write_molcas(filename, array_xyz, array_charges):
    write_molpro(filename, array_xyz, array_charges, molcas=True)
    return


def write_molpro(filename, array_xyz, array_charges, molcas=False):

    with open(filename, "w") as out:
        if not molcas:
            print("Point charges in Molpro format", file=out)
        print(len(array_charges), 0, "Angstrom", file=out)
        for xyz, charge in zip(array_xyz, array_charges):
            data = list(xyz)+[charge]
            # lat_format = "{: 14.8f} {: 14.8f} {: 14.8f} {: 16.10f} 0"
            lat_format = 4 * "{: 15.9f} " + "0"
            print(lat_format.format(*data),
                  file=out)
    return


def read_xyz(filename, delete_labels=True):
    with open(filename) as f:
        length = int(f.readline())
    array_labels = numpy.loadtxt(filename, skiprows=2, dtype=str, usecols=0)
    assert array_labels.shape[0] == length
    if delete_labels:
        array_labels = numpy.array([''.join(
            filter(lambda x: x.isalpha(), label)) for label in array_labels])
    array_xyz = numpy.loadtxt(filename, skiprows=2, usecols=(1, 2, 3))
    try:
        array_charges = numpy.loadtxt(filename, skiprows=2, usecols=4)
    except(IndexError):
        array_charges = None
    return array_xyz, array_charges, array_labels


def read_molpro(filename, molcas=False):
    if molcas:
        n_skip = 1
    else:
        n_skip = 2
    with open(filename) as f:
        f.readline()
        length = int(f.readline()[0])
    array = numpy.loadtxt(filename, skiprows=n_skip, usecols=(0, 1, 2, 3))
    print(array.shape)
    assert array.shape[0] == length
    array_xyz = array[:, 0:3]
    array_charges = array[:, 3]
    return array_xyz, array_charges
