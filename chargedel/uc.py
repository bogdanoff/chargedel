#!/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2019, 2020 Nikolay Bogdanov <n.bogdanov@inbox.lv>

import numpy
import shlex


class unitcell(object):
    """Unit cell to construct point charge embedding.

       attributes: abc_cartesian, ion_fractional, ion_charges, ion_labels
    """
    def __init__(self,
                 abc_cartesian=[],
                 ion_labels=[],
                 ion_fractional=[],
                 ion_charges=[],
                 tol=1e-12,
                 filename=None,
                 data_dict=None,
                 ewaldinp=None,
                 ciffile=None,
                 verbose=False):
        self.abc_cartesian = numpy.array(abc_cartesian)
        self.ion_fractional = numpy.array(ion_fractional)
        self.ion_charges = numpy.array(ion_charges)
        self.ion_labels = numpy.array(ion_labels)
        self.tol = tol
        self.verbose = verbose
        init_sources = [filename is None, data_dict is None, ewaldinp is None,
                        ciffile is None]
        assert init_sources.count(False) < 2, ">1 init sources given"
        if filename is not None:
            self.init_from_input(filename)
        if data_dict is not None:
            self.init_from_dict(data_dict)
        if ewaldinp is not None:
            self.init_from_ewaldinp(ewaldinp)
        if ciffile is not None:
            self.init_from_cif(ciffile)

    @property
    def ion_cartesian(self):
        return numpy.dot(self.ion_fractional, self.abc_cartesian)

    @ion_cartesian.setter
    def ion_cartesian(self, ion_xyz):
        self.ion_fractional = numpy.dot(ion_xyz, self.uvw_reciprocal)

    @property
    def uvw_reciprocal(self):
        return numpy.linalg.inv(self.abc_cartesian).T

    @uvw_reciprocal.setter
    def uvw_reciprocal(self, uvw):
        self.abc_cartesian = numpy.linalg.inv(uvw).T

    @property
    def volume(self):
        return abs(numpy.linalg.det(self.abc_cartesian))

    def print(self):
        print("abc_cartesian\n", self.abc_cartesian)
        print("ion_fractional\n", self.ion_fractional)
        print("ion_charges\n", self.ion_charges)
        print("ion_labels\n", self.ion_labels)

    def shift_origin_frac_(self, new_origin):
        frac_new = self.ion_fractional - numpy.array(new_origin)
        frac_new = frac_new.round(decimals=int(-numpy.log10(self.tol)))
        self.ion_fractional = frac_new - frac_new // 1

    def abc_rotated_with_matrix(self, matrix):
        return numpy.dot(self.abc_cartesian, matrix)

    def abc_rotated(self, angle, axis=2):
        ang = numpy.radians(angle)
        rot_mat = numpy.eye(3)
        for x in (axis - 2, axis - 1):
            rot_mat[x, x] = numpy.cos(ang)
        rot_mat[axis - 2, axis - 1] = numpy.sin(ang)
        rot_mat[axis - 1, axis - 2] = -numpy.sin(ang)
        # rotZ = numpy.array([[numpy.cos(ang), numpy.sin(ang), 0.0],
        #                  [-numpy.sin(ang), numpy.cos(ang), 0.0],
        #                  [0.0, 0.0, 1.0]])
        return self.abc_rotated_with_matrix(rot_mat)

    def rotate_cartesian_abc_(self, angle, axis=2):
        self.abc_cartesian = self.abc_rotated(angle, axis)

    def abc_from_lattice_abc(self, a, b, c, alpha, beta, gamma):
        self.abc_cartesian = numpy.array(cartesian_vectors(a, b, c, alpha,
                                                           beta, gamma))

    def abc_from_lattice_dict(self, data_dict):
        a = data_dict["a"]
        b = data_dict["b"]
        c = data_dict["c"]
        alpha = data_dict["alpha"]
        beta = data_dict["beta"]
        gamma = data_dict["gamma"]
        self.abc_from_lattice_abc(a, b, c, alpha, beta, gamma)

    def init_from_dict(self, data_dict):
        if "abc_cartesian" in data_dict:
            self.abc_cartesian = numpy.array(data_dict["abc_cartesian"])
        else:
            self.abc_from_lattice_dict(data_dict)
        self.ion_fractional = numpy.array(data_dict["ion_fractional"])
        try:
            self.ion_charges = numpy.array(data_dict["ion_charges"])
        except(KeyError):
            print("No charges available for ions in the UC")
        self.ion_labels = numpy.array(data_dict["ion_labels"])

    def init_from_input(self, filename):
        data_dict = read_input(filename)
        self.init_from_dict(data_dict)

    def init_from_ewaldinp(self, filename):
        data_dict = read_ewaldinp(filename)
        self.init_from_dict(data_dict)

    def init_from_cif(self, ciffile):
        data_dict = read_cif(ciffile, verbose=self.verbose)
        self.init_from_dict(data_dict)

    def set_charges(self, charges_dict):
        self.ion_charges = numpy.array([charges_dict[ion] for ion in
                                        self.ion_labels])


def cartesian_vectors(a, b, c, alpha, beta, gamma):
    (alpha, beta, gamma) = (numpy.radians(angle)
                            for angle in (alpha, beta, gamma))
    # Following code from Openbabel
    # https://github.com/openbabel/openbabel/blob/master/src/formats/cifformat.cpp
    v = numpy.sqrt(1 - numpy.cos(alpha)**2 - numpy.cos(beta)**2 -
                   numpy.cos(gamma)**2 +
                   2 * numpy.cos(alpha) * numpy.cos(beta) * numpy.cos(gamma))
    # Required reciprocal parameters
    rc = numpy.sin(gamma) / c / v
    ralpha = numpy.arccos(
        (numpy.cos(beta) * numpy.cos(gamma) - numpy.cos(alpha)) /
        (numpy.sin(beta) * numpy.sin(gamma)))
    abc_cartesian = [[a, 0, 0], [
        b * numpy.cos(gamma), b * numpy.sin(gamma), 0
    ], [c * numpy.cos(beta), -c * numpy.sin(beta) * numpy.cos(ralpha), 1 / rc]]
    return numpy.array(abc_cartesian)


def make_uc_from_dict(data_dict):
    a = data_dict["a"]
    b = data_dict["b"]
    c = data_dict["c"]
    alpha = data_dict["alpha"]
    beta = data_dict["beta"]
    gamma = data_dict["gamma"]
    abc_cartesian = cartesian_vectors(a, b, c, alpha, beta, gamma)
    return unitcell(abc_cartesian=abc_cartesian)


def add_value_(data_dict, line, verbose=False):
    key = line.split()[0].split('_')[-1]
    value = float(line.split()[-1])
    if verbose:
        print(key, value)
    data_dict[key] = value
    return


def read_abc_from_cif(filename):
    data_dict = {}
    with open(filename, "r") as f:
        for line in f:
            if "_cell_length" in line or "_cell_angle" in line:
                add_value_(data_dict, line)
    return (data_dict)


def readcif_str(filename):
    cif_dict = {}
    read_loop_arguments = False
    read_loop_data = False
    read_multiline = False
    with open(filename, "r") as f:
        for line in f:
            if not line.strip() or line.strip()[0] == "#":
                continue
            if "loop_" in line.lower():
                loop_arguments = []
                read_loop_arguments = True
                read_loop_data = False
                continue
            if read_loop_arguments:
                if line.strip().startswith("_"):
                    argument_label = line.strip()[1:]
                    loop_arguments.append(argument_label)
                    cif_dict[argument_label] = []
                    continue
                else:
                    read_loop_arguments = False
                    read_loop_data = True
            if read_loop_data:
                if not line.strip().startswith("_"):
                    line_data = shlex.split(line)
                    for (key, value) in zip(loop_arguments, line_data):
                        cif_dict[key].append(value)
                    continue
                else:
                    read_loop_data = False
            if "data_" in line.lower():
                cif_dict["data"] = line.strip()[5:]
                continue
            if line.strip().startswith("_"):
                argument_label = line.split()[0][1:]
                if len(shlex.split(line)) == 2:
                    cif_dict[argument_label] = shlex.split(line)[1]
                    continue
                else:
                    read_multiline = True
            if read_multiline:
                if line.strip().startswith(";"):
                    if argument_label in cif_dict:
                        read_multiline = False
                        continue
                    else:
                        continue
                else:
                    if argument_label in cif_dict:
                        cif_dict[argument_label] += line.strip()
                        continue
                    else:
                        cif_dict[argument_label] = line.strip()
                        continue
            print("Can't read this:", line)
    return cif_dict


def data_from_cif_dict(cif_dict):
    if int(cif_dict["space_group_IT_number"]) != 1:
        raise NotImplementedError("Convert cif file to P1 space group")
    data_dict = {}
    for data in ["ion_labels", "ion_fractional", "ion_charges"]:
        data_dict[data] = []
    for label in "abc":
        data_dict[label] = float(cif_dict["cell_length_" + label])
    for angle in ["alpha", "beta", "gamma"]:
        data_dict[angle] = float(cif_dict["cell_angle_" + angle])
    data_dict["ion_labels"] = [''.join(filter(str.isalpha, symbol))
                               for symbol in cif_dict["atom_site_type_symbol"]]
    for ion_xyz in zip(cif_dict["atom_site_fract_x"],
                       cif_dict["atom_site_fract_y"],
                       cif_dict["atom_site_fract_z"]):
        data_dict["ion_fractional"].append([float(coord) for coord in ion_xyz])
    data_dict["ion_fractional"] = numpy.array(data_dict["ion_fractional"])
    if ("atom_type_symbol" in cif_dict.keys() and
            "atom_type_oxidation_number" in cif_dict.keys()):
        charge_dict = {}
        for symbol, oxydation in zip(cif_dict["atom_type_symbol"],
                                     cif_dict["atom_type_oxidation_number"]):
            charge_dict[symbol] = float(oxydation)
            data_dict["ion_charges"] = []
        for symbol in cif_dict["atom_site_type_symbol"]:
            data_dict["ion_charges"].append(charge_dict[symbol])
    return data_dict


def read_cif(filename, verbose=False):
    cif_dict = readcif_str(filename)
    if verbose:
        print(cif_dict)
    return data_from_cif_dict(cif_dict)


def read_input(filename):
    data_dict = {}
    with open(filename, "r") as f:
        line = f.readline()
        while line:
            if 'uc' in line:
                (data_dict['a'], data_dict['b'], data_dict['c']) = \
                    (float(item) for item in f.readline().split())
                (data_dict["alpha"], data_dict["beta"], data_dict["gamma"]) = \
                    (float(item) for item in f.readline().split())
            if 'ions{' in line:
                ion_labels = []
                ion_fractional = []
                ion_charges = []
                line = f.readline()
                while '}' not in line:
                    data = line.split()
                    assert len(data) == 5
                    ion_labels.append(data[0])
                    ion_fractional.append(
                        numpy.array([float(coor) for coor in data[1:4]]))
                    ion_charges.append(float(data[4]))
                    line = f.readline()
                data_dict["ion_labels"] = ion_labels
                data_dict["ion_fractional"] = numpy.array(ion_fractional)
                data_dict["ion_charges"] = numpy.array(ion_charges)
            line = f.readline()
    return data_dict


def read_ewaldinp(filename):
    data_dict = {}
    for data in ["ion_labels", "ion_fractional", "ion_charges"]:
        data_dict[data] = []
    with open(filename, "r") as f:
        data_dict["abc_cartesian"] = [[float(num)
                                       for num in f.readline().split()[0:3]]
                                      for line in range(3)]
        for line in f:
            if len(line) > 1:
                data = line.split()
                assert len(data) == 5
                data_dict["ion_fractional"].append([float(coor) for coor in
                                                    data[0:3]])
                data_dict["ion_charges"].append(float(data[3]))
                data_dict["ion_labels"].append(data[4])
    return data_dict
