#!/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2019 Nikolay Bogdanov <n.bogdanov@inbox.lv>
#

import numpy as np
import itertools as it
from numba import njit  # , jit, prange
from chargedel import evjen

R_BOHR = 0.52917721092
TINY = 5e-10


def gen_pc_depreceted(uc, n_cells=3, fill_up=True, extra_layers=2,
                      verbose=False):
    pc_array_frac = []
    pc_array_charges = []
    cells_range = range(-n_cells, n_cells)
    for (xtrans, ytrans, ztrans) in it.product(cells_range, repeat=3):
        pc_array_frac.extend(uc.ion_fractional + (xtrans, ytrans, ztrans))
        pc_array_charges.extend(uc.ion_charges)
    if fill_up:
        extra_range = range(-n_cells - extra_layers, n_cells + extra_layers)
        for (xtrans, ytrans, ztrans) in it.product(extra_range, repeat=3):
            if all(trans in cells_range for trans in [xtrans, ytrans, ztrans]):
                continue
            for coords, charge in zip(
                    uc.ion_fractional + (xtrans, ytrans, ztrans),
                    uc.ion_charges):
                if (coords >= -n_cells).all() and (coords <= n_cells).all():
                    if verbose:
                        print("fill", (xtrans, ytrans, ztrans), coords, charge)
                    pc_array_frac.append(coords)
                    pc_array_charges.append(charge)
    pc_array_xyz = np.dot(pc_array_frac, uc.abc_cartesian)
    return pc_array_xyz, pc_array_charges


def gen_pc(uc, n_cells=3, apply_evjen=True, general_evjen=None, fill_up=True,
           extra_layers=2, verbose=False, sort=True, origin=[0, 0, 0],
           scale_gen_evjen=True):
    pc_array_frac = []
    pc_array_charges = []
    pc_array_labels = []
    if type(n_cells) == int:
        n_cells = np.array([n_cells] * 3)
    if type(n_cells) == list:
        assert len(n_cells) == 3
        n_cells = np.array(n_cells)
    if type(n_cells) == np.ndarray:
        assert n_cells.shape == (3, )
    if general_evjen is not None:
        assert len(general_evjen) == 2
        if scale_gen_evjen:
            abc = np.array([np.linalg.norm(coord) for coord in uc.abc_cartesian])
            abc_factors = np.min(abc)/abc
            n_cells_partial_abc = [int(np.ceil(general_evjen[0]*factor))
                                   for factor in abc_factors]
            n_cells_formal_abc = [int(np.ceil(general_evjen[1]*factor))
                                  for factor in abc_factors]
            if (abc_factors < 1.0).any():
                print("The following number of partial and formal cells are used:",
                      n_cells_partial_abc, n_cells_formal_abc)
            gen_evjen_charge = \
                [evjen.pc_renorm_fun(n_cells_partial_abc[i], n_cells_formal_abc[i])
                 for i in range(3)]
            for i, n_dir in enumerate(n_cells):
                assert n_dir >= n_cells_partial_abc[i] + n_cells_formal_abc[i]
        else:
            n_cells_partial = general_evjen[0]
            n_cells_formal = general_evjen[1]
            gen_evjen_charge = evjen.pc_renorm_fun(n_cells_partial, n_cells_formal)
            # gen_evjen_charge.debug()
            for n_dir in n_cells:
                assert n_dir >= n_cells_partial + n_cells_formal
    if fill_up:
        cells_range = [
            range(-n_dir - extra_layers, n_dir + extra_layers)
            for n_dir in n_cells
        ]
    else:
        cells_range = [range(-n_dir, n_dir) for n_dir in n_cells]
    for (atrans, btrans, ctrans) in it.product(*cells_range):
        for coords, charge, elem in zip(
                uc.ion_fractional + np.array([atrans, btrans, ctrans]),
                uc.ion_charges, uc.ion_labels):
            if (coords >= -n_cells).all() and (coords <= n_cells).all():
                if apply_evjen:
                    if general_evjen is None:
                        charge = evjen.evjen_charge(coords, charge,
                                                    [-n_cells, n_cells])
                    else:
                        # charge0 = charge
                        if scale_gen_evjen:
                            for i, coord in enumerate(coords):
                                charge = gen_evjen_charge[i].dim1(coord)*charge
                        else:
                            charge = gen_evjen_charge.multidim(coords)*charge
                        # print(elem, charge0, charge, coords, np.linalg.norm(coords))
                        # for coord in coords:
                        #    print(coord, gen_evjen_charge.dim1(coord))
                        # if abs(charge) - abs(charge0) > TINY:
                        #     for coord in coords:
                        #         print(coord, gen_evjen_charge.dim1(coord))
                        #     raise(RuntimeError)
                if abs(charge) > TINY:
                    pc_array_frac.append(coords)
                    pc_array_charges.append(charge)
                    pc_array_labels.append(elem)
                    # print(elem, np.linalg.norm(coords), charge)
    pc_array_xyz = np.dot(pc_array_frac, uc.abc_cartesian)
    pc_array_charges = np.array(pc_array_charges)
    if sort:
        pc_array_xyz, pc_array_charges, pc_array_labels = \
            sort_pc(pc_array_xyz, pc_array_charges, pc_array_labels,
                    origin=origin)
    return pc_array_xyz, pc_array_charges, pc_array_labels


# idea: generate array of charges by taking all complete uc's within a sphere
# Evjen modification should be possible too according to uc indices


def sum_xyz(array):
    return np.sum(array, axis=1)


def xyz_metric(array):
    dist_squared = np.multiply(array, array).sum(1)
    # return np.sort(dist_squared)
    # return np.sort(sum_xyz(array))
    ind_sort = np.lexsort((array[:, 0], array[:, 1], array[:, 2], sum_xyz(array), dist_squared))
    return array[ind_sort]


@njit
def verify_arrays(array1, array2, tol=1e-12):
    in_array = 0  # np.zeros(array1.shape[0], dtype=bool)
    dim = array1.shape[0]
    for i in range(dim):
        for j in range(dim):
            # if np.allclose(array1[i], array2[j]):
            if (
                    array1[i, 0] - array2[j, 0] < tol and
                    array1[i, 1] - array2[j, 1] < tol and
                    array1[i, 2] - array2[j, 2] < tol):
                in_array += 1
                break
    if in_array == array1.shape[0]:
        return True
    else:
        return False


def sym_generators():
    sym_gen = {}
    sym_gen["x"] = np.array([-1, 1, 1])
    sym_gen["y"] = np.array([1, -1, 1])
    sym_gen["z"] = np.array([1, 1, -1])
    for (key1, key2) in [("x", "y"), ("x", "z"), ("y", "z")]:
        sym_gen[key1 + key2] = sym_gen[key1] * sym_gen[key2]
    sym_gen["xyz"] = np.array([-1, -1, -1])
    return sym_gen


def detect_symmetry(pc_array_xyz):
    # reference = xyz_metric(pc_array_xyz)
    found_sym = []
    sym_gen = sym_generators()
    for key in sym_gen.keys():
        sym_array = pc_array_xyz * sym_gen[key]
        # test = xyz_metric(sym_array)
        # if np.allclose(test, reference
        if verify_arrays(sym_array, pc_array_xyz):
            found_sym.append(key)
    return found_sym


def sort_pc(pc_array_xyz, pc_array_charges, pc_array_labels, origin=[0, 0, 0]):
    vecs_to_origin = np.array(pc_array_xyz) - np.array(origin)
    distances = np.linalg.norm(vecs_to_origin, axis=1)
    order = np.argsort(distances)
    sorted_xyz = np.array(pc_array_xyz)[order]
    if pc_array_charges is not None:
        sorted_charges = np.array(pc_array_charges)[order]
    else:
        sorted_charges = None
    if pc_array_labels is not None:
        sorted_labels = np.array(pc_array_labels)[order]
    else:
        sorted_labels = None
    return sorted_xyz, sorted_charges, sorted_labels


def find_xyz_in_array(xyz, array_xyz, Bohr=False):
    # (index, component) = numpy.where(array_xyz == xyz)
    if Bohr:
        xyz = xyz * R_BOHR
    index = None
    for i, xyz_i in enumerate(array_xyz):
        if np.allclose(xyz, xyz_i):
            return i
    return index


def subtract_cluster(cluster_xyz, full_array_xyz, full_array_charges,
                     full_array_labels, cluster_labels=None):
    pc_array_xyz = np.array(full_array_xyz).copy()
    pc_array_charges = np.array(full_array_charges).copy()
    pc_array_labels = np.array(full_array_labels).copy()
    for i, xyz in enumerate(cluster_xyz):
        index = find_xyz_in_array(xyz, pc_array_xyz)
        if cluster_labels is not None:
            assert pc_array_labels[index] == cluster_labels[i]
        pc_array_xyz = np.delete(pc_array_xyz, index, 0)
        pc_array_charges = np.delete(pc_array_charges, index, 0)
        pc_array_labels = np.delete(pc_array_labels, index, 0)
    assert pc_array_xyz.shape[0] == \
        full_array_xyz.shape[0] - cluster_xyz.shape[0]
    return pc_array_xyz, pc_array_charges, pc_array_labels


def find_cluster(array_xyz, array_labels, centers=[["Cu"], ["O"]],
                 origin=[0, 0, 0], spheres=[2.5, 2.65], tol=0.1):
    origin = np.array(origin)
    sorted_xyz, _, sorted_labels = sort_pc(array_xyz, None, array_labels,
                                           origin=origin)
    clust_xyz = np.array([])
    clust_labels = []
    closest_dist = 1E8
    for ixyz, ilabel in zip(sorted_xyz, sorted_labels):
        dist = np.linalg.norm(ixyz - origin)
        if dist > closest_dist + tol:
            break
        if ilabel in centers[0]:
            if closest_dist == 1E8:
                closest_dist = np.linalg.norm(ixyz-origin)
                clust_xyz = ixyz
            else:
                clust_xyz = np.vstack((clust_xyz, ixyz))
            clust_labels.append(ilabel)
    for center, sphere in zip(centers, spheres):
        for c_xyz, c_lab in zip(clust_xyz, clust_labels):
            if c_lab in center:
                distances = np.linalg.norm(sorted_xyz-c_xyz, axis=1)
                within_sphere = distances <= sphere
                clust_xyz = np.vstack((clust_xyz,
                                       sorted_xyz[within_sphere]))
                clust_labels.extend(sorted_labels[within_sphere])
        clust_xyz, uindices = np.unique(clust_xyz, axis=0, return_index=True)
        clust_labels = [clust_labels[idx] for idx in uindices]
    sorted_clust_xyz, _, sorted_clust_labels = sort_pc(clust_xyz, None,
                                                       clust_labels,
                                                       origin=origin)
    return (sorted_clust_xyz, sorted_clust_labels)
