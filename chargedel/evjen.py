#!/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2019, 2022 Nikolay Bogdanov <n.bogdanov@inbox.lv>
#

import numpy as np


def evjen_charge(frac_coords, charge, minmax_list):
    assert len(minmax_list) == 2, "one min and one max index or np.array"
    coords = np.array(frac_coords)
    for item in minmax_list:
        if (isinstance(item, type(coords))):
            assert item.shape == coords.shape, "minmax not int nor np.array(3)"
        else:
            assert type(
                item) == int, "expect min and max be int or np.array(3)"
    count_minmax = sum([np.sum(coords == val) for val in minmax_list])
    assert count_minmax <= 3, "Check your min and max: " + str(minmax_list)
    return (charge / 2**count_minmax)


def pad_poly_with_zeros(poly_list):
    """
    As sympy not always returns full-length list of polynomial coefficients, we
    need to pad those lists (usually only the last one) with zeros
    """
    good_length = len(poly_list[0])
    for i, poly in enumerate(poly_list):
        if len(poly) != good_length:
            poly_list[i] = poly_list[i] + [0.0]*(good_length - len(poly))
    return poly_list


def ih_polynom(n_part, n_form, sign):
    """
    Generate a complete set of polynomils to define a uniform sum distribution
    utilized by A. Gell\'{e} and M.-B. Lepetit J. Chem. Phys. 128, 244716 (2008)
    Definition here follows https://oeis.org/A188668
    f[n_, k_, c_] := f[n, k, c] = 1/(n!)*(Sum[(-1)^j*Binomial[n, j]*(y - j)^n,
                                              {j, 0, k}]) /. {y->(x + c/2 + n)}

    n_part: number of cells with partial charges (l in the paper)
    n_form: number of cells with formal charges starting from origin
            (probably n' in the paper)
    sign: -1 (1) correspond to the lower (higher) edge of the charge array
    """
    from sympy.abc import x, c, k
    from sympy import Sum, symbols, binomial, factorial
    from sympy import Poly
    assert sign == 1 or sign == -1
    n, j = symbols("n j", nonnegative=True)
    y = -sign*x + c/2 + n
    irwing_hall_cdf = Sum(((-1)**j)*binomial(n, j)*(y-j)**n,
                          (j, 0, k))/factorial(n)

    poly_list = [
        list(Poly(irwing_hall_cdf.subs({n: n_part, k: kk, c: n_form*2}).doit().n(),
                  x).all_coeffs()[::-1])
        for kk in range(0, n_part+1)]
    return pad_poly_with_zeros(poly_list)


class pc_renorm_fun(object):
    """
    Function returning the normalization coefficnients for point charges
    proposed in [ A. Gell\'{e} and M.-B. Lepetit J. Chem. Phys. 128, 244716 (2008),
    https://doi.org/10.1063/1.2931458 ].
    attributes:
        n_partial: int, number of unit cells with partial charges
        n_formal: int, number of unit cells with formal charges
        polynom_negative: list of lists of reals, define the polynomial
                          function for renormalization for coord < 0
        polynom_positive: list of lists of reals, define the polynomial
                          function for renormalization for coord > 0
    """
    def __init__(self,
                 n_partial=None,
                 n_formal=None,
                 polynom_negative=[],
                 polynom_positive=[]):
        self.n_partial = n_partial
        self.n_formal = n_formal
        self.polynom_negative = ih_polynom(n_partial, n_formal, -1)
        self.polynom_positive = ih_polynom(n_partial, n_formal, 1)
    
    @property
    def polynom_list(self):
        return [self.polynom_negative, self.polynom_positive]

    def dim1(self, x):
        """
        Return renormalization factor for point charges in one dimension
        Eq. (45) and Fig. 2 in J. Chem. Phys. 128, 244716 (2008),
        doi:10.1063/1.2931458
    
        """
        sign = int(np.sign(x))
        if sign < 0:
            poly_list = self.polynom_negative
        else:
            poly_list = self.polynom_positive
        if sign*x >= self.n_partial + self.n_formal:
            return 0.0
        elif sign*x < self.n_formal:
            return 1.0
        else:
            range_low_boundary = int(-x*sign + self.n_formal)
            range_polynom = poly_list[self.n_partial + range_low_boundary - 1]
        factor = sum([range_polynom[i]*x**(i)
                      for i in range(len(range_polynom))])
        return np.float64(factor)

    def multidim(self, coord_list):
        # Return renormalization factor for point charges in several dimensions
        factor = 1.0
        for x in coord_list:
            factor *= self.dim1(x)
        return factor

    def debug(self):
        import matplotlib.pyplot as plt
        lim = self.n_partial+self.n_formal+1
        points = np.linspace(-lim, lim, 20*lim + 1)
        y = [self.dim1(p) for p in points]
        plt.plot(points, y, color='b')
        plt.show()
        plt.close()


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    my_p = 10
    my_f = 1
    points = np.linspace(-my_p-my_f-0.2, my_p+my_f+0.2, 1001)
    my_ren = pc_renorm_fun(my_p, my_f)
    y = [my_ren.dim1(p) for p in points]
    plt.plot(points, y, color='b')
    plt.show()
    plt.close()
