#!/usr/bin/env python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Copyright 2019 Nikolay Bogdanov <n.bogdanov@inbox.lv>
#

# import sys
import numpy
import itertools as it
from math import erfc
from numba import njit  # , jit, prange
from chargedel import gen_array


ZERO = 0.0
TINY = 1e-10
EL_SQUARED = 14.399764  # Squared elementary charge, eV * Angstrom
EL = numpy.sqrt(EL_SQUARED)


@njit
def direct_sum(xyz, array_xyz, array_charges):
    potential = ZERO
    # xyz = numpy.array(xyz)
    # array_dist = numpy.sqrt(numpy.sum(numpy.square(array_xyz - xyz),axis=1))
    # array_dist can contain zero => it's probably cheaper to do a loop
    for one_xyz, one_charge in zip(array_xyz, array_charges):
        dist = numpy.sqrt(numpy.dot(xyz - one_xyz, xyz - one_xyz))
        if dist > TINY:
            potential += one_charge * EL / dist
    return potential


def ewald_sum(xyz, uc, eta=None, max_real=None, max_recip=None,
              precision=(TINY / 10), verbose=False):
    eta, max_real, max_recip = ewald_sum_parameters(uc, eta, max_real,
                                                    max_recip, precision)
    if verbose:
        print("eta {} max_real {} max_recip {}".format(eta, max_real,
                                                       max_recip))
    return ewald_sum_old(xyz, uc, eta=eta, max_real=max_real,
                         max_recip=max_recip)
#    return ewald_sum_old(xyz, uc)


def ewald_sum_old(xyz, uc, eta=0.2, max_real=8, max_recip=5, verbose=False):
    # uc_transl, uc_xyz, uc_charges, uc_reciprocal,
    ewald_real = 0
    xyz_charge = 0.0
    pc_array_xyz, pc_array_charges, pc_array_labels = gen_array.gen_pc(
        uc, n_cells=max_real)
    for array_xyz, array_charge in zip(pc_array_xyz, pc_array_charges):
        dist = numpy.sqrt(numpy.dot(xyz - array_xyz, xyz - array_xyz))
        if dist > TINY:
            ewald_real += erfc(eta * dist) * array_charge * EL / dist
        else:
            # print(array_xyz, array_charge)
            assert(xyz_charge == 0.0), 'More then two charges at ' + str(xyz)
            xyz_charge = array_charge

    ewald_recipr = 0
    for uc_ion_xyz, uc_ion_charge in zip(uc.ion_cartesian, uc.ion_charges):
        for utrans in range(-max_recip, max_recip):
            for vtrans in range(-max_recip, max_recip):
                for wtrans in range(-max_recip, max_recip):
                    if utrans == vtrans == wtrans == 0:
                        continue
                    mtrans = numpy.dot([utrans, vtrans, wtrans],
                                       numpy.array(uc.uvw_reciprocal))
                    mtrans_squared = numpy.dot(mtrans, mtrans)
                    fact1 = uc_ion_charge * EL / (numpy.pi * uc.volume)
                    fact2 = numpy.exp(-mtrans_squared * (numpy.pi / eta)**2)
                    fact3 = numpy.cos(2 * numpy.pi *
                                      numpy.dot(mtrans, xyz - uc_ion_xyz))
                    ewald_recipr += fact1 * fact2 * fact3 / mtrans_squared
    ewald_const = -2. * eta * xyz_charge * EL / numpy.sqrt(numpy.pi)
    ewald_total = ewald_real + ewald_recipr + ewald_const
    if verbose:
        print("Tot {} Real {} Recip {} Const {} Charge {}".format(ewald_total,
                                                                  ewald_real,
                                                                  ewald_recipr,
                                                                  ewald_const,
                                                                  xyz_charge))
    return ewald_total


def ewald_sum_parameters(uc, eta, max_real, max_recip, precision):
    # Optimal eta and precision estimate according to \cite{optimalEwald}
    real_average = numpy.sqrt(numpy.average((uc.abc_cartesian**2).sum(1)))
    recip_average = numpy.sqrt(numpy.average((uc.uvw_reciprocal**2).sum(1)))
    if eta is None:
        eta = (numpy.sqrt(numpy.pi) * uc.ion_charges.shape[0]**(1 / 6) /
               real_average)
    if max_real is None:
        max_real = int(
            numpy.ceil(numpy.sqrt(-numpy.log(precision)) / eta / real_average))
    while numpy.exp(-real_average * (max_real * eta)**2) > precision:
        max_real += 1
    if max_recip is None:
        max_recip = int(
            numpy.ceil(
                numpy.sqrt(-numpy.log(precision)) * eta / numpy.pi /
                recip_average))
    while numpy.exp(-recip_average *
                    (max_recip * numpy.pi / eta)**2) > precision:
        max_recip += 1
    return eta, max_real, max_recip


def ewald_real():
    return


# WIP
def ewald_sum_new(xyz, uc, eta=None, max_real=None, max_recip=None,
                  precision=(TINY / 10), verbose=True):
    eta, max_real, max_recip = ewald_sum_parameters(uc, eta, max_real,
                                                    max_recip, precision)
    if verbose:
        print("eta {} max_real {} max_recip {}".format(eta, max_real,
                                                       max_recip))
        
    ewald_real = 0
    xyz_charge = 0.0
    pc_array_xyz, pc_array_charges = gen_array.gen_pc(uc,
                                                      n_cells=max_real,
                                                      fill_up=False)
    for array_xyz, array_charge in zip(pc_array_xyz, pc_array_charges):
        dist = numpy.sqrt(numpy.dot(xyz - array_xyz, xyz - array_xyz))
        if dist > TINY:
            ewald_real += erfc(eta * dist) * array_charge * EL / dist
        else:
            # print(array_xyz, array_charge)
            assert (xyz_charge == 0.0), 'More then two charges at ' + str(xyz)
            xyz_charge = array_charge  # Check this, maybe +=

    ewald_recipr = 0
    for uc_ion_xyz, uc_ion_charge in zip(uc.ion_cartesian, uc.ion_charges):
        for utrans in range(-max_recip, max_recip):
            for vtrans in range(-max_recip, max_recip):
                for wtrans in range(-max_recip, max_recip):
                    if utrans == vtrans == wtrans == 0:
                        continue
                    mtrans = numpy.dot([utrans, vtrans, wtrans],
                                       numpy.array(uc.uvw_reciprocal))
                    mtrans_squared = numpy.dot(mtrans, mtrans)
                    fact1 = uc_ion_charge * EL / (numpy.pi * uc.volume)
                    fact2 = numpy.exp(-mtrans_squared * (numpy.pi / eta)**2)
                    fact3 = numpy.cos(2 * numpy.pi *
                                   numpy.dot(mtrans, xyz - uc_ion_xyz))
                    ewald_recipr += fact1 * fact2 * fact3 / mtrans_squared
    ewald_const = -2. * eta * xyz_charge * EL / numpy.sqrt(numpy.pi)
    ewald_total = ewald_real + ewald_recipr + ewald_const
    return ewald_total, ewald_real, ewald_recipr, ewald_const, xyz_charge


def dip_mom(xyz_array, charges_array):
    dipole = numpy.dot(charges_array, xyz_array)
    second = numpy.dot(charges_array, numpy.square(xyz_array))
    dip_squared = numpy.dot(dipole, dipole)
    sec_squared = numpy.sqrt(numpy.dot(second, second))
    energy_elstat = 0
    for (xyz1, ch1), (xyz2,
                      ch2) in it.combinations(zip(xyz_array, charges_array),
                                              2):
        dist_squared = numpy.dot(xyz1 - xyz2, xyz1 - xyz2)
        assert (dist_squared > 0.001), 'some ions are too close'
        energy_elstat += ch1 * ch2 * EL_SQUARED / numpy.sqrt(dist_squared)
    return (dipole, dip_squared, second, sec_squared, energy_elstat)
